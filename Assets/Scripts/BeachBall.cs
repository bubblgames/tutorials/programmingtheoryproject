using UnityEngine;

public class BeachBall : Ball
{
    
    void Update()
    {
        Move();

        if (Input.GetKey(KeyCode.Space))
        {
            Jump();
        }
    }

    // Update is called once per frame
    public override void Jump()
    {
        transform.Translate(Vector3.up);
    }
}
