using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    private float speed = 3f;
    protected Rigidbody _rigidbody;
    private bool onGround = false;
    [SerializeField] private float jumpForce;
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.Space) && onGround)
        {
            Jump();
        }
    }

    public void Move()
    {
        var horizontalInput = Input.GetAxis("Horizontal");
        var verticalInput = Input.GetAxis("Vertical");

        transform.Translate(Vector3.forward * verticalInput * Time.deltaTime * speed);
        transform.Translate(Vector3.right * horizontalInput * Time.deltaTime * speed);
    }

    public virtual void Jump()
    {
        _rigidbody.AddForce(Vector3.up * jumpForce);
        onGround = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        onGround = true;
    }
}
